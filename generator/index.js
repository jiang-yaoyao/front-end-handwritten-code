function* foo(num) {
  console.log('函数开始执行……')
  let value1 = 500
  let a = yield value1
  console.log(a)

  let value2 = 400 * a
  let b = yield value2
  console.log(b)

  let value3 = 300 * a
  let c = yield value2
  console.log(c)

  console.log('函数执行完毕')
}

let generator = foo(1)
let a = generator.next(1)
console.log(a)
let b = generator.next(2)
console.log(b)
let c = generator.next(3)
console.log(c)

function* createArrIterator (arr) {
  for(const item of arr){
    yield item
  }
}

let name = ['abc','def','f']
let nameIterator = createArrIterator(name)
console.log(nameIterator.next())

function* createArrIterator2(arr) {
  yield* arr
}

let nameIterator2 = createArrIterator2(name)
console.log(nameIterator2.next())

function createRangeIterator(start,end){
  let index = start
  return {
    next: function(){
      if(index < end){
        return {done: false,value: index++}
      }else {
        return {done: true,value: undefined}
      }
    }
  }
}

let range = createRangeIterator(10,20)
console.log(range.next())

function* createRangeIterator2(start,end){
  let index = start
  if(index < end){
    yield index++
  }
}
let range2 = createRangeIterator2(10,20)
console.log(range2.next())
