 // Vue3的数据劫持通过Proxy函数对代理对象的属性进行劫持，通过Reflect对象里的方法对代理对象的属性进行修改，
 // Proxy代理对象不需要遍历，配置项里的回调函数可以通过参数拿到修改属性的键和值
 // 这里用到了Reflect对象里的三个方法，get，set和deleteProperty，方法需要的参数与配置项中回调函数的参数相同。
 // Reflect里的方法与Proxy里的方法是一一对应的，只要是Proxy对象的方法，就能在Reflect对象上找到对应的方法。
 const obj = {
  name: '刘逍',
  age: 20
}
const p = new Proxy(obj, {
  // 读取属性的时候会调用getter
  get(target, propName) {  //第一个参数为代理的源对象,等同于上面的Obj参数。第二个参数为读取的那个属性值
    console.log(`有人读取p对象里的${propName}属性`);
    return Reflect.get(target, propName)
  },
  // 添加和修改属性的时候会调用setter
  set(target, propName, value) { //参数等同于get，第三个参数为修改后的属性值
    console.log(`有人修改了p对象里的${propName}属性,值为${value},需要去修改视图`);
    Reflect.set(target, propName, value)
  },
  // 删除属性时，调用deleteProperty
  deleteProperty(target, propName) { // 参数等同于get
    console.log(`有人删除了p对象里的${propName}属性，需要去修改视图`);
    return Reflect.deleteProperty(target, propName)
  }
})