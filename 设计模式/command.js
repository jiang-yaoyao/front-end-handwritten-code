class Receiver {
  execute() {
    console.log('接收者执行请求')
  }
}

class Command {
  constructor(receiver) {
    this.receiver = receiver
  }
  execute() {
    this.receiver.execute()
  }
}

class Invoker {
  constructor(command){
    this.command = command
  }
  invoker() {
    this.command.execute()
  }
}

const warehose = new Receiver()
const order = new Command(warehose)
const client = new Invoker(order)
client.invoker()