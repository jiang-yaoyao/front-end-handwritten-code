class Iterator {
  constructor(list) {
    this.list = list
    this.index = 0
  }
  next() {
    if(this.getNext()){
      // return this.list[this.index++]
      console.log(this.list[this.index++])
    }
  }
  getNext() {
    if(this.index < this.list.length){
      return true
    }
    return false
  }
}
class Container {
  constructor(list) {
    this.list = list
  }
  getIterator() {
    return new Iterator(this.list)
  }
}

let list = new Container([1,2,3,4,5,6])
let iterator = list.getIterator()

while(iterator){
  iterator.next()
}