class Olympiad {
  constructor () {
    this.players = []
  }
  join(player) {
    this.players.push(player)
  }
  exit(player) {
    this.players.splice(this.players.findIndex(item => item.name === player.name),i)
  }
  getResult() {
    console.log('参赛所有方',this.players)
    this.players.forEach(item => {
      console.log(item.name,item.state)
    })
  }
}

class player {
  constructor(name) {
    this.name = name
    this.state = 'ready'
  }
  lose() {
    this.state = 'lose'
  }
  win() {
    this.state = 'win'
  }
}
const rabbit = new player('兔子')
const bear = new player('北极熊')
const chicken = new player('鸡')

const olympiad = new Olympiad()
olympiad.join(rabbit)
olympiad.join(bear)
olympiad.join(chicken)

rabbit.win()
bear.win()
chicken.lose()

olympiad.getResult()