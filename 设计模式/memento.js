class Memento {
  constructor(content){
    this.content = content
  }
  getContent() {
    return this.content
  }
}

class CarTaker {
  constructor() {
    this.list = []
  }
  add(memento) {
    this.list.push(memento)
  }
  get(index) {
    return this.list[index]
  }
  getList() {
    return this.list
  }
}

class Editor {
  constructor() {
    this.content = null
  }
  setContent(content) {
    this.content = content
  }
  getContent() {
    return this.content
  }
  saveContentToMemento() {
    return new Memento(this.content)
  }
  getConentFromMemento(memento) {
    this.content = memento.getContent()
  }
}

let editor = new Editor()
let carTaker = new CarTaker()

editor.setContent('111')
editor.setContent('222')

carTaker.add(editor.saveContentToMemento()) // 将当前222内容备份
editor.setContent('333')
carTaker.add(editor.saveContentToMemento()) // 将当前333内容备份
editor.setContent('444')

console.log(editor.getContent())
editor.getConentFromMemento(carTaker.get(1)) // 撤销
