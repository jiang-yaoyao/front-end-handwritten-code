let observer_ids = 0
let observed_ids = 0

class observer {
  constructor() {
    this.id = observer_ids++
  }
  update(ob) {
    console.log(`观察者${this.id},被观察者${ob}`)
  }
}

class observed {
  constructor() {
    this.observer_list = []
    this.id = observed_ids++
  }
  addObserver(ob) {
    this.observer_list.push(ob)
  }
  removeObserver(ob) {
    this.observer_list.filter(o => {
      return o.id != ob.id
    })
  }
  notify(ob) {
    this.observer_list.forEach(o=> {
      o.update(ob)
    })
  }
}

let mObserved = new observed()
console.log(mObserved)
let mObserved1 = new observer()
let mObserved2 = new observer()

mObserved.addObserver(mObserved1)
mObserved.addObserver(mObserved2)

mObserved.notify()