class eventBus {
  constructor() {
    this.events = new Map()
  }
  subscribe(eventname,fn) {
    let fns = this.events.get(eventname)
    if(!fns){
      fns = []
      fns.push(fn)
      this.events.set(eventname,fns)
    }else {
      fns.push(fn)
    }
  }
  publish(...args) {
    let eventname = args.shift()
    let fns = this.events.get(eventname)
    for(let i = -1; fns[++i];){
      fns[i](args)
    }
  }
}
var eventBus1 = new eventBus()
eventBus1.subscribe(3000,function(data) {
  console.log(data)
})
eventBus1.publish(3000,'发布了一个3000的房子')