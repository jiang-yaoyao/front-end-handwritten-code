let strategies = {
  'S': function(salary){
    return salary*4
  },
  'A': function(salary){
    return salary*3
  },
  'B': function(salary){
    return salary*2
  },
  'A': function(salary){
    return salary*1
  }
}

let calculateBonus = function(level,salary){
  return strategies[level](salary)
}

calculateBonus('S',10000)