var Visitor = (function() {
  return {
    splice: function(arguments) {
      var args = Array.prototype.splice.call(arguments,1)
      return Array.prototype.splice.apply(arguments[0],args)
    },
    push: function(arguments) {
      var len = arguments[0].length || 0
      var args = this.splice(arguments,1)
      arguments[0].length = len + arguments.length -1
      return Array.prototype.push.apply(arguments[0],args)
    },
    pop: function(arguments) {
      return Array.prototype.pop.apply(arguments[0])
    }
  }
})()

var a = {}
Visitor.push(a,1,2,3)
Visitor.push(a,4,5,6)
Visitor.pop(a)
Visitor.splice(a,1)